import { environment } from '../../environments/environment';

export class AuthEndpoint {
  public static login = `${environment.backUrl}api/login`;
}
