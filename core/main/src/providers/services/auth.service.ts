import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthEndpoint } from '../endpoints/auth.endpoint';
import { ApiService } from 'core/shared/src/helpers/api';
import { Observable } from 'rxjs';
import { StorageService } from 'core/shared/src/helpers/util';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private apiService: ApiService,
    private router: Router,
    private storageService: StorageService
  ) { }


  logout(): void {
    this.storageService.clear();
    this.router.navigate(['/login']);
  }

  login(params: any, showSpin: boolean = false): Observable<boolean> {
    return this.apiService.post(AuthEndpoint.login, params, { preloader: showSpin });
  }


}
