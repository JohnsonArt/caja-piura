import { animate, animateChild, AnimationTriggerMetadata, query, stagger, style, transition, trigger } from '@angular/animations';

export function itemsTransition(): AnimationTriggerMetadata {
  return trigger('items', [
    transition(':enter', [
      style({ transform: 'scale(0.9)', opacity: 0 }),  // initial
      animate('0.3s ease-in',
        style({ transform: 'scale(1)', opacity: 1 }))  // final
    ])
  ]);
}

export function listTransition(): AnimationTriggerMetadata {
  return trigger('list', [
    transition(':enter', [
      query('@items', stagger(300, animateChild()), { optional: true })
    ])
  ]);
}
