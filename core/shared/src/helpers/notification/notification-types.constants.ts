export const NOTIFICATION_TYPES = {
  success: {
    code: 'S',
    color: 'success',
    icon: 'la-check-circle'
  },
  alert: {
    code: 'A',
    color: 'alert',
    icon: ''
  },
  warning: {
    code: 'W',
    color: 'warning',
    icon: 'la-warning'
  },
  message: {
    code: 'M',
    color: 'message',
    icon: ''
  },
  error: {
    code: 'E',
    color: 'danger',
    icon: 'la-warning'
  }
};
