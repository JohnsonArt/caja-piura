import { DatePipe } from '@angular/common';

export class DateUtil {

  constructor(public datePipe: DatePipe) { }

  public static dateRange(startDate: string, endDate: string): Array<string> {
    const start = startDate.split('-');
    const end = endDate.split('-');
    const startYear = parseInt(start[0], 10);
    const endYear = parseInt(end[0], 10);
    const dates = [];

    for (let i = startYear; i <= endYear; i++) {
      const endMonth = i !== endYear ? 11 : parseInt(end[1], 10) - 1;
      const startMon = i === startYear ? parseInt(start[1], 10) - 1 : 0;
      for (let j = startMon; j <= endMonth; j = j > 12 ? j % 12 || 11 : j + 1) {
        const month = j + 1;
        const displayMonth = month < 10 ? `0${month}` : month;
        dates.push([i, displayMonth, '01'].join('-'));
      }
    }

    return dates;
  }

  public static stringDDMMYYYYToDate(date: string | any): Date {
    if (!date) { return null; }
    const buff = this.splitDate(date);

    return new Date([buff[1], buff[0], buff[2]].join('/'));

  }

  public static stringYYYYMMDDToDate(date: string | any): Date {
    if (!date) { return null; }
    const buff = this.splitDate(date);

    return new Date(parseInt(buff[0], 10), parseInt(buff[1], 10) - 1, parseInt(buff[2], 10));
  }

  public static splitDate(date: string): object {
    return date.includes('/') ? date.split('/') : date.split('-');
  }

  public static formatDateYYYYDDMMtoDDMMYYY(dateStr: string): string {
    return dateStr.split('-')
      .reverse()
      .join('/');
  }

  public static formatDateYYYYMMDD(date): string {
    const dd = String(date.getDate())
      .padStart(2, '0');
    const mm = String(date.getMonth() + 1)
      .padStart(2, '0');
    const yyyy = date.getFullYear();

    return `${yyyy}-${mm}-${dd}`;
  }

  public static isGreaterThanDate(date1: Date, date2: Date): boolean {
    return date1 > date2;
  }

  public static betweenDate(dateFrom: Date, dateTo: Date, dateCheck: Date): boolean {
    return dateCheck >= dateFrom && dateCheck <= dateTo;
  }

  public static isExpired(endDate: Date): boolean {
    const today = new Date();

    return today > endDate;
  }

  public static setTimezoneDate(date: any): Date {
    if (date) { return new Date(`${date}${date.endsWith('Z') ? '' : '-00:00'}`); } else { return new Date(); }
  }
}
