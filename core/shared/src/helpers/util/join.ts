export const joinSelected = (array: Array<any>) => {
    return array.filter(item => item.selected)
    .map(item => {
      return item.value;
    })
    .join(',');
};
