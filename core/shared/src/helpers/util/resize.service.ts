import { Injectable } from '@angular/core';
import { BehaviorSubject, fromEvent } from 'rxjs';

export const SCREEN_NAMES = {
  XS: 'xs',
  SM: 'sm',
  MD: 'md',
  LG: 'lg',
  XL: 'xl'
};

export const SCREEN_RESOLUTIONS: Array<IScreenResolutions> = [
  { type: SCREEN_NAMES.XS, min: 0, max: 576 },
  { type: SCREEN_NAMES.SM, min: 576, max: 767 },
  { type: SCREEN_NAMES.MD, min: 768, max: 991 },
  { type: SCREEN_NAMES.LG, min: 992, max: 1199 },
  { type: SCREEN_NAMES.XL, min: 1200, max: 5120 }
];

const _window = (): any => {
  return window;
};

@Injectable({
  providedIn: 'root'
})
export class ResizeService {
  currentWidth: number;
  currentHeigh: number;
  resizeEvent: BehaviorSubject<number>;
  constructor() {
    this.currentWidth = this.getNativeWindow().innerWidth;
    this.currentHeigh = this.getNativeWindow().innerHeight;
    this.resizeEvent = new BehaviorSubject<number>(this.currentWidth);
    fromEvent(this.getNativeWindow(), 'resize')
      .subscribe((event: any) => {
        this.currentWidth = event.target.innerWidth;
        this.currentHeigh = event.target.innerHeight;
        this.resizeEvent.next(this.currentWidth);
      });
  }

  getNativeWindow(): any {
    return _window();
  }

  getEvent(): BehaviorSubject<number> {
    return this.resizeEvent;
  }

  is(screenType: string): boolean {
    return screenType === this.getScreenType();
  }

  isEqual(screenType: string, width: number): boolean {
    return screenType === this.getScreenType(width);
  }

  getScreenType(width?: number): string {
    if (!width) { width = this.currentWidth; }
    const screen = SCREEN_RESOLUTIONS.find(sr => sr.min <= width && width <= sr.max);

    return screen ? screen.type : '';
  }
}

export interface IScreenResolutions {
  type: string;
  min: number;
  max: number;
}
