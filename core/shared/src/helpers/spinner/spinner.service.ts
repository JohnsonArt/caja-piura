// import { ComponentPortal, DomPortalHost } from '@angular/cdk/portal';
// import { ApplicationRef, ComponentFactoryResolver, Injectable, Injector } from '@angular/core';
// import { SpinnerComponent } from './spinner.component';

// @Injectable()
// export class SpinnerService {

//   private spinnerPortalHost: ComponentPortal<SpinnerComponent>;
//   private bodyPortalHost: DomPortalHost;

//   constructor(
//     private componentFactoryResolver: ComponentFactoryResolver,
//     private applicationRef: ApplicationRef,
//     private injector: Injector
//   ) {
//     // Create ComponentPortal that can be attached to a DomPortalHost
//     this.spinnerPortalHost = new ComponentPortal(SpinnerComponent);
//     // Create a DomPortalHost with document.body as its anchor element
//     this.bodyPortalHost = new DomPortalHost(
//       document.body,
//       this.componentFactoryResolver,
//       this.applicationRef,
//       this.injector);
//   }

//   showSpinner(): void {
//     // Attach the ComponentPortal to the DomPortalHost.
//     try {
//       this.bodyPortalHost.attach(this.spinnerPortalHost);
//     } catch (error) { }
//   }

//   hideSpinner(): void {
//     // Detach the ComponentPortal from the DomPortalHost
//     this.bodyPortalHost.detach();
//   }
// }

import { Injectable } from '@angular/core';
import { SpinnerComponent } from './spinner.component';

@Injectable()
export class SpinnerService {


  constructor(
  ) {}

  showSpinner(): void {
    //show spinner
  }

  hideSpinner(): void {
    //hide spinner
  }
}

