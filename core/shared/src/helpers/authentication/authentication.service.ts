import { Injectable } from '@angular/core';
import { USER_TOKEN_KEY } from './authentication.constant';
import { StorageService } from '../util';

@Injectable()
export class AuthenticationService {

  constructor(
    private _storageService: StorageService
  ) { }

  setUserToken(token: string): void {
    this._storageService.setItem(USER_TOKEN_KEY, token);
  }

  getUserToken(): IUserToken {
    const data = this._storageService.getItem(USER_TOKEN_KEY);

    return data ? JSON.parse(data) : null;
  }

  isAuthenticated(): boolean {
    return !!this._storageService.getItem(USER_TOKEN_KEY);
  }

}

export interface IUserToken {
  username: string;
  rememberMe: boolean;
  id_token: string;
}
