import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { NotificationModule } from '../notification';
import { ApiService } from './api.service';
import { SpinnerModule } from '../spinner';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    NotificationModule,
    SpinnerModule
  ],
  providers: [
    ApiService
  ],
  exports: [
    HttpClientModule
  ]
})
export class ApiModule { }
