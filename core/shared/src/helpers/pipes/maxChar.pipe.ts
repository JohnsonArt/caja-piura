import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'maxChar'})
export class maxCharPipe implements PipeTransform {
  transform(value: string, length = 300): string {
    return  `${value.substr(0, length)}...(ver más)`;
  }

}
