import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { GroupByPipe } from './group-by.pipe';
import { maxCharPipe } from './maxChar.pipe';
import { SafeHtmlPipe } from './safe-html.pipe';
import { SafeUrlPipe } from './safe-url.pipe';
@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FileSizePipe,
    GroupByPipe,
    SafeHtmlPipe,
    SafeUrlPipe,
    maxCharPipe
  ],
  exports: [
    FileSizePipe,
    GroupByPipe,
    SafeHtmlPipe,
    SafeUrlPipe,
    maxCharPipe
  ]
})
export class PipesModule { }
