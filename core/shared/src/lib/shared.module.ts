import { NgModule } from '@angular/core';
import { SharedComponent } from './shared.component';
import { ButtonComponent } from './button/button.component';



@NgModule({
  declarations: [SharedComponent, ButtonComponent],
  imports: [
  ],
  exports: [SharedComponent]
})
export class SharedModule { }
