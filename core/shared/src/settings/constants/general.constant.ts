export const GENERAL = {
  info: 'example'
}

export const STORAGE_KEYS = {
  userToken: 'USER_TOKEN',
  userInfo: 'USER_INFO',
}

export const GENERAL_LANGS = {
  notifications: {
    success: 'Se realizó correctamente',
    notSaveCorrectly: 'No se guardo correctamente',
    completeFields: 'Completa los campos',
    existIncorrectFields: 'Existen campos incorrectos',
    thereAreChangesWithoutSaving: 'Existen cambios sin guardar'
  }
};