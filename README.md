# CajaPiura

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.20.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# LIBRERIA DE COMPONENTES COMUNES EXTERNOS

Los componentes que estan siendo utilizados por los componentes web, estan siendo empaquetados en una unica libreria. La cual esta siendo maneja como un proyecto de angular en 'core/lib/', dentro de este estan anidados los componentes externos, para agregar un nuevo debe seguirse el siguiente proceso:

  - Generar componente como un proyecto de angular, ubicarlo dentro de la carpeta 'core/lib/'.
  - Incluir dentro de este un archivo que sirva para exporta su modulo, componente, metodos, etc. Se puede colocar uno que tenga como nombre 'public_api.ts', este deberia tener el siguiente codigo:

      `export * from '<nombre del modulo o componente>'`

  - Despues este archivo debe incluirse de la misma forma en el archivo 'public_api.ts' que se encuenta el directorio 'src'.

  - Una vez terminado, en consola ejecutar la siguiente linea de comandos para compilar y ubicar la libreria dentro del proyecto bvlsite:

      `npm run build:lib`

  - Ahora, el web component que importe algun componente externo, debera importarlo de la siguiente forma:

    `import { <nombre del componente> } from '@cp/library'